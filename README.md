This is the code for my personal website.

There are two subfolders:

myServer: Builds a binary which runs on a Linux machine at my house to fetch samples from a LimeSDR, process them, and send them off to the website.

ryanjamesallandotcom: Builds a flutter app which runs on the client.

Both subfolders include their own instructions.